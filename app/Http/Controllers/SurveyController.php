<?php


/*
|--------------------------------------------------------------------------
| Survey Controller for the Talking Toilet
|--------------------------------------------------------------------------
|*/

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
 
use Input;
use Redirect;

class SurveyController extends Controller
{
    /**
     * Display a the Toilet welcome page and form.
     *
     * @return Response
     */
    public function index()
    {
        //
    }


    /**
     * Display a the Toilet thank you page with stats
     *
     * @return Response
     */
    public function thanks()
    {
        // Save the input and display output view with stats on thank you page
        $input = Input::all(); 
        \App\Survey::create( $input );


        // fetch stats to display
        $num_surveys = \App\Survey::count();
        $num_newspaper = \App\Survey::where('paper', '=', 'Newspaper')->count();
        $num_toiletpaper = $num_surveys - $num_newspaper;
        $num_crumpled = \App\Survey::where('method', '=', 'Crumpled')->count();
        $num_folded = $num_surveys - $num_crumpled;

        // work out some percentages 
        $percent_newspaper = number_format( $num_newspaper/$num_surveys * 100, 2 ) . '%';        
        $percent_toiletpaper = number_format( $num_toiletpaper/$num_surveys * 100, 2 ) . '%';        
        $percent_crumpled = number_format( $num_crumpled/$num_surveys * 100, 2 ) . '%';        
        $percent_folded = number_format( $num_folded/$num_surveys * 100, 2 ) . '%';        
        
        // work out highest word occurence out of "Thinking about" messages entered
        $surveys = \App\Survey::All();
        $all_messages = '';
        foreach($surveys as $survey){
            $all_messages .= trim($survey->message).' ';
        }
        $words = array_count_values(str_word_count($all_messages, 1));
        arsort($words); 
        $average_thought_words = '';
        $i = 0;
        foreach($words as $index=>$value){
            $i++; 
            $average_thought_words .= $index.', ';
            if ($i == 5) break;
        }
        $average_thought_words .= 'etc.';        

        return view('thanks')
            ->with('percent_newspaper', $percent_newspaper)
            ->with('percent_toiletpaper', $percent_toiletpaper)
            ->with('percent_folded', $percent_folded)
            ->with('percent_crumpled', $percent_crumpled)
            ->with('average_thought_words', $average_thought_words); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //        
        return view('welcome'); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {  
        //     
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
