<!DOCTYPE html>
<html>
    <head>
        <title>The Talking Toilet App</title>

        <link href="//fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

        <style>
            html, body {
                height: 100%;
            }

            html { 
              background: url(bg.jpg) no-repeat center center fixed; 
              -webkit-background-size: cover;
              -moz-background-size: cover;
              -o-background-size: cover;
              background-size: cover;
            }            

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }

            .container {
                text-align: left;
                display: table-cell;
                vertical-align: top;
            }

            .content {
                text-align: left;
                margin-left: 40px;
                display: inline-block;
            }

            .title {
                font-size: 96px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <h1 class="title">Talking Toilet.</h1>

                <h2>Thank you for your submission, overall stats as follows:</h2>


                <h3>What paper do people prefer?</h3>                
                Toilet Paper <b>{{ $percent_toiletpaper }}</b><br>
                Newspaper <b>{{ $percent_newspaper }}</b><br>

                <h3>What method do people prefer?</h3>                
                Folded <b>{{ $percent_folded }}</b><br>
                Crumpled <b>{{ $percent_crumpled }}</b><br>

                <h3>Most people are thinking of the following:</h3>

                <b>{{ $average_thought_words }}</b>

                <br><br><br>

                <b><< <a href="/">ONE MORE FLUSH, PLEASE.</a></b>
 
            </div>
        </div>
    </body>
</html>
