<!DOCTYPE html>
<html>
    <head>
        <title>The Talking Toilet App</title>

        <link href="//fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">


        <style>
            html, body {
                height: 100%;
            }

            html { 
              background: url(bg.jpg) no-repeat center center fixed; 
              -webkit-background-size: cover;
              -moz-background-size: cover;
              -o-background-size: cover;
              background-size: cover;
            }            

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 100;
                font-family: 'Helvetica';
            }

            .container {
                text-align: left;
                display: table-cell;
                vertical-align: top;
            }

            .content {
                text-align: left;
                margin-left: 40px;
                display: inline-block;
            }

            .title {
                font-size: 96px;
            }

            form {
                opacity: 0.8;
                background-color: #FFFFFF;                
            }

            label {
                margin-top: 20px;
                font-weight: bold;
            }


            .paper-choice {
                width:40%;
                float: left;
            }

            .choice-method {
                width:40%;
                float: left;
            }

        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <h1 class="title">Talking Toilet.</h1>

                <h2>Capture Form:</h2>


                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>

                {!! Form::open(array('route' => 'thanks', 'class' => 'form')) !!}

               <div class="form-group paper-choice">
                    <b>{!! Form::label('Choice of paper?') !!}</b>
                    <br><br>
                    {!! Form::radio('paper', 'Toilet Paper', true)  !!} Toilet Paper <br><br>
                    {!! Form::radio('paper', 'Newspaper')  !!} Newspaper <br><br> 
                </div>

                <div class="form-group choice-method">
                    <b>{!! Form::label('Method of choice?') !!}</b>
                    <br><br>
                    {!! Form::radio('method', 'Folded', true) !!} Folded<br><br>
                    {!! Form::radio('method', 'Crumpled') !!} Crumpled<br><br> 
                </div>

                <br clear="both">
                <br>
                <br>
                <!--
                <div class="form-group">
                    {!! Form::label('Your Name') !!}
                    {!! Form::text('name', null, 
                        array('required', 
                              'class'=>'form-control', 
                              'placeholder'=>'Your name')) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('Your E-mail Address') !!}
                    {!! Form::text('email', null, 
                        array('required', 
                              'class'=>'form-control', 
                              'placeholder'=>'Your e-mail address')) !!}
                </div>
                -->

                <div class="form-group">
                    <b>{!! Form::label('What are you thinking of?') !!}</b>
                    <br>
                    {!! Form::textarea('message', null, 
                        array('required', 
                              'class'=>'form-control', 
                              'placeholder'=>'Work, deadlines, etc.')) !!}
                </div>

                <div class="form-group">
                    {!! Form::submit('Submit Results', 
                      array('class'=>'btn btn-primary')) !!}
                </div>
                {!! Form::close() !!}

            </div>
        </div>
    </body>
 
    </script>
</html>
